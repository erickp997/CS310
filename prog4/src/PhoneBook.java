/* Program 4
 * Erick Pulido
 * cssc0934
 */
import data_structures.*;
import java.util.Iterator;
import java.io.*;
public class PhoneBook {
    private int size;
    private DictionaryADT<PhoneNumber,String> book;
    /* Constructor builds a PhoneBook of user specified size */
    public PhoneBook(int maxSize) {
        size = maxSize;
        book = new Hashtable<PhoneNumber,String>(size);
        //book = new BinarySearchTree<PhoneNumber,String>();
        //book = new BalancedTree<PhoneNumber,String>();
    }
    /* 
     * Read the data from a text file in the form key=value and                    
     * add each entry to the PhoneBook. PhoneNumber corresponds to
     * key and name "Last, First" is the paired value.
     */
    public void load(String filename) {
        String text;
        try {
            BufferedReader in = new BufferedReader(new FileReader(filename));
            while ((text = in.readLine()) != null) {
                PhoneNumber pnEntry = new PhoneNumber(text.substring(0,12));
                String nameEntry = text.substring(13);
                addEntry(pnEntry, nameEntry);
            }
            in.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }           
    /*
     * Return the name value associated with the PhoneNumber if it
     * exists in the PhoneBook, otherwise return null.
     */
    public String numberLookup(PhoneNumber number) {
        return book.getValue(number);
    }
    /*
     * Return the PhoneNumber key of the key=value pairing. In the case of
     * duplicates the first one found is returned, otherwise return null.
     */
    public PhoneNumber nameLookup(String name) {
        return book.getKey(name);
    }
    /*
     * Add the PhoneNumber=name pair to the PhoneBook only if the entry is not a 
     * duplicate. Returns true if insertion succeeds, false if it does not whether
     * the PhoneBook is full or a duplicate was found. Does not modify the file.
     */
    public boolean addEntry(PhoneNumber number, String name) {
        return book.add(number, name);
    }
    /*
     * Delete the PhoneNumber=key pair in the PhoneBook if it exists. Return true
     * if the PhoneNumber was found and the pair was deleted, otherwise false.
     * Does not modify the file.
     */
    public boolean deleteEntry(PhoneNumber number) {
        return book.delete(number);
    }
    /*
     * Prints all of the PhoneNumber=name pairs, in sorted order which is ordered
     * by PhoneNumber.
     */
    public void printAll() {
        Iterator<PhoneNumber> numbers = book.keys();
        while(numbers.hasNext()) {
            PhoneNumber tmpKey = numbers.next();
            String key = tmpKey.toString();
            System.out.println(key + "=" + book.getValue(tmpKey));
        }       
    }
    /*
     * Prints all the PhoneNumber=name pairs that match the area code provided.
     * These are sorted by PhoneNumber.
     */
    public void printByAreaCode(String code) {
        Iterator<PhoneNumber> numbers = book.keys();
        while (numbers.hasNext()) {  // Search and find given area code
            PhoneNumber currentNumber = numbers.next();
            String valueAreaCode = book.getValue(currentNumber);
            PhoneNumber test = currentNumber; 
            int comparison = code.compareTo(test.areaCode);
            if (comparison == 0) // print if matches with given area code
                System.out.println(test.phoneNumber + "=" + valueAreaCode);
        }   
    }
    /*
     * Prints all the name entries in PhoneBook which are sorted by name.
     * Duplicates are allowed for these values.
     */   
    public void printNames() {
        Iterator<String> names = book.values();
        String [] arrayOfNames = new String[book.size()];
        int index = 0;
        while (names.hasNext()) {
            arrayOfNames[index++] = names.next();
        }
        sortNames(arrayOfNames);
        for(int i = 0; i < arrayOfNames.length; i++) {
            System.out.println(arrayOfNames[i]);
        }
    }
    /* Performs Shell Sort to get the names in alphabetical order. */
    private void sortNames(String[] array) {
        String tmp;
        int in, out, h = 1;
        int size = array.length;
        while (h <= size/3) // calculate the gaps
            h = h * 3 + 1;
        while (h > 0) {
            for(out = h; out < size; out++) {
                tmp = array[out];
                in = out;
                while (in > h-1 && array[in-h].compareTo(tmp) >= 0) {
                    array[in] = array[in-h];
                    in-=h;
                }
                array[in] = tmp;
            }
            h = (h-1)/3;
        }
    }
}