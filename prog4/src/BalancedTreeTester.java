import java.util.Iterator;
import java.util.ConcurrentModificationException;
import data_structures.*;

public class BalancedTreeTester {
    public static void main(String [] args) {
        // don't put on edoras
        final int SIZE = 1000000;
        long start, stop;
        int [] array = new int[SIZE];
        DictionaryADT<Integer,Integer> dictionary = 
          //new Hashtable<Integer,Integer>(SIZE);
          new BinarySearchTree<Integer,Integer>();
          //new BalancedTree<Integer,Integer>();      
        
        if(dictionary.contains(20)) 	System.out.println("There is no 20");
        if(dictionary.contains(9)) 		System.out.println("There is no 9");
        if(dictionary.contains(53)) 	System.out.println("There is no 53");
        if(dictionary.contains(100))	System.out.println("There is no 100");
        if(dictionary.contains(2)) 		System.out.println("There is no 2");
        if(dictionary.contains(45)) 	System.out.println("There is no 45");
        if(dictionary.contains(17)) 	System.out.println("There is no 17");
        if(dictionary.contains(60)) 	System.out.println("There is no 60");
        if(dictionary.contains(32)) 	System.out.println("There is no 32");
        if(dictionary.contains(23)) 	System.out.println("There is no 23");
        if(dictionary.contains(120))	System.out.println("There is no 120");
        if(dictionary.contains(300))	System.out.println("There is no 300");
        
        if(!dictionary.add(20, 20)) 	System.out.println("Did not add 20");
        if(!dictionary.add(9, 9)) 		System.out.println("Did not add 9");
        if(!dictionary.add(53, 53)) 	System.out.println("Did not add 53");
        if(!dictionary.add(100, 100)) 	System.out.println("Did not add 100");
        if(!dictionary.add(2, 2)) 		System.out.println("Did not add 2");
        if(!dictionary.add(45, 45)) 	System.out.println("Did not add 45");
        if(!dictionary.add(17, 17)) 	System.out.println("Did not add 17");
        if(!dictionary.add(60, 60)) 	System.out.println("Did not add 60");
        if(!dictionary.add(32, 32)) 	System.out.println("Did not add 32");
        if(!dictionary.add(23, 23))	 	System.out.println("Did not add 23");
        if(!dictionary.add(120, 120)) 	System.out.println("Did not add 120");
        if(!dictionary.add(300, 300)) 	System.out.println("Did not add 300");
        
        if(!dictionary.contains(20)) 	System.out.println("There is 20");
        if(!dictionary.contains(9)) 	System.out.println("There is 9");
        if(!dictionary.contains(53)) 	System.out.println("There is 53");
        if(!dictionary.contains(100))	System.out.println("There is 100");
        if(!dictionary.contains(2)) 	System.out.println("There is 2");
        if(!dictionary.contains(45)) 	System.out.println("There is 45");
        if(!dictionary.contains(17)) 	System.out.println("There is 17");
        if(!dictionary.contains(60)) 	System.out.println("There is 60");
        if(!dictionary.contains(32)) 	System.out.println("There is 32");
        if(!dictionary.contains(23)) 	System.out.println("There is 23");
        if(!dictionary.contains(120)) 	System.out.println("There is 120");
        if(!dictionary.contains(300)) 	System.out.println("There is 300");

        
        if(!dictionary.delete(20)) 		System.out.println("Did not delete 20");
        if(!dictionary.delete(9)) 		System.out.println("Did not delete 9");
        if(!dictionary.delete(53)) 		System.out.println("Did not delete 53");
        if(!dictionary.delete(100)) 	System.out.println("Did not delete 100");
        if(!dictionary.delete(2)) 		System.out.println("Did not delete 2");
        if(!dictionary.delete(45)) 		System.out.println("Did not delete 45");
        if(!dictionary.delete(17)) 		System.out.println("Did not delete 17");
        if(!dictionary.delete(60)) 		System.out.println("Did not delete 60");
        if(!dictionary.delete(32)) 		System.out.println("Did not delete 32");
        if(!dictionary.delete(23)) 		System.out.println("Did not delete 23");
        if(!dictionary.delete(120)) 	System.out.println("Did not delete 120");
        if(!dictionary.delete(300)) 	System.out.println("Did not delete 300");
        
        
        if(!dictionary.isEmpty())		System.out.println("The dictionary is empty");
        
        
        if(dictionary.contains(20)) 	System.out.println("There is no 20");
        if(dictionary.contains(9)) 		System.out.println("There is no 9");
        if(dictionary.contains(53)) 	System.out.println("There is no 53");
        if(dictionary.contains(100))	System.out.println("There is no 100");
        if(dictionary.contains(2)) 		System.out.println("There is no 2");
        if(dictionary.contains(45)) 	System.out.println("There is no 45");
        if(dictionary.contains(17)) 	System.out.println("There is no 17");
        if(dictionary.contains(60)) 	System.out.println("There is no 60");
        if(dictionary.contains(32)) 	System.out.println("There is no 32");
        if(dictionary.contains(23)) 	System.out.println("There is no 23");
        if(dictionary.contains(120))	System.out.println("There is no 120");
        if(dictionary.contains(300))	System.out.println("There is no 300");
        
        
        if(!dictionary.add(20, 20)) 	System.out.println("Did not add 20");
        if(!dictionary.add(9, 9)) 		System.out.println("Did not add 9");
        if(!dictionary.add(53, 53)) 	System.out.println("Did not add 53");
        if(!dictionary.add(100, 100)) 	System.out.println("Did not add 100");
        if(!dictionary.add(2, 2)) 		System.out.println("Did not add 2");
        if(!dictionary.add(45, 45)) 	System.out.println("Did not add 45");
        if(!dictionary.add(17, 17)) 	System.out.println("Did not add 17");
        if(!dictionary.add(60, 60)) 	System.out.println("Did not add 60");
        if(!dictionary.add(32, 32)) 	System.out.println("Did not add 32");
        if(!dictionary.add(23, 23))	 	System.out.println("Did not add 23");
        if(!dictionary.add(120, 120)) 	System.out.println("Did not add 120");
        if(!dictionary.add(300, 300)) 	System.out.println("Did not add 300");
        
        
        if(dictionary.getValue(20) != 20) 	System.out.println("Didn't return value 20");
        if(dictionary.getValue(9)  != 9) 	System.out.println("Didn't return value 9");
        if(dictionary.getValue(53) != 53) 	System.out.println("Didn't return value 53");
        if(dictionary.getValue(100)!= 100)	System.out.println("Didn't return value 100");
        if(dictionary.getValue(2)  != 2) 	System.out.println("Didn't return value 2");
        if(dictionary.getValue(45) != 45) 	System.out.println("Didn't return value 45");
        if(dictionary.getValue(17) != 17) 	System.out.println("Didn't return value 17");
        if(dictionary.getValue(60) != 60) 	System.out.println("Didn't return value 60");
        if(dictionary.getValue(32) != 32) 	System.out.println("Didn't return value 32");
        if(dictionary.getValue(23) != 23) 	System.out.println("Didn't return value 23");
        if(dictionary.getValue(120)!= 120)  System.out.println("Didn't return value 120");
        if(dictionary.getValue(300)!= 300)  System.out.println("Didn't return value 300");
        
        
        if(dictionary.getKey(20) != 20) 	System.out.println("Didn't return key 20");
        if(dictionary.getKey(9)  != 9) 		System.out.println("Didn't return key 9");
        if(dictionary.getKey(53) != 53) 	System.out.println("Didn't return key 53");
        if(dictionary.getKey(100)!= 100)	System.out.println("Didn't return key 100");
        if(dictionary.getKey(2)  != 2) 		System.out.println("Didn't return key 2");
        if(dictionary.getKey(45) != 45) 	System.out.println("Didn't return key 45");
        if(dictionary.getKey(17) != 17) 	System.out.println("Didn't return key 17");
        if(dictionary.getKey(60) != 60) 	System.out.println("Didn't return key 60");
        if(dictionary.getKey(32) != 32) 	System.out.println("Didn't return key 32");
        if(dictionary.getKey(23) != 23) 	System.out.println("Didn't return key 23");
        if(dictionary.getKey(120)!= 120) 	System.out.println("Didn't return key 120");
        if(dictionary.getKey(300)!= 300) 	System.out.println("Didn't return key 300");
        
        
        if(dictionary.size() != 12)			System.out.println("Didn't return correct size 12");
        
      
        if(dictionary.isFull())				System.out.println("The dictionary is not full with only 12 items");	
        
        
        if(dictionary.isEmpty())			System.out.println("The dictionary is not empty");
        
        if(dictionary.getValue(21) != null) System.out.println("21 isn't a key");
        if(dictionary.getValue(11) != null) System.out.println("11 isn't a key");
        if(dictionary.getValue(54) != null) System.out.println("54 isn't a key");
        if(dictionary.getValue(101)!= null) System.out.println("101 isn't a key");
        if(dictionary.getValue(3)  != null) System.out.println("3 isn't a key");
        if(dictionary.getValue(46) != null) System.out.println("46 isn't a key");
        if(dictionary.getValue(18) != null) System.out.println("18 isn't a key");
        if(dictionary.getValue(61) != null) System.out.println("61 isn't a key");
        if(dictionary.getValue(33) != null) System.out.println("33 isn't a key");
        if(dictionary.getValue(24) != null) System.out.println("24 isn't a key");
        if(dictionary.getValue(121)!= null) System.out.println("121 isn't a key");
        if(dictionary.getValue(301)!= null) System.out.println("301 isn't a key");
        
        
        if(dictionary.getKey(21)  != null) 	System.out.println("21 isn't a value");
        if(dictionary.getKey(11)  != null) 	System.out.println("11 isn't a value");
        if(dictionary.getKey(54)  != null) 	System.out.println("54 isn't a value");
        if(dictionary.getKey(101) != null)	System.out.println("101 isn't a value");
        if(dictionary.getKey(3)   != null) 	System.out.println("3 isn't a value");
        if(dictionary.getKey(46)  != null) 	System.out.println("46 isn't a value");
        if(dictionary.getKey(18)  != null) 	System.out.println("18 isn't a value");
        if(dictionary.getKey(61)  != null) 	System.out.println("61 isn't a value");
        if(dictionary.getKey(33)  != null) 	System.out.println("33 isn't a value");
        if(dictionary.getKey(24)  != null) 	System.out.println("24 isn't a value");
        if(dictionary.getKey(121) != null) 	System.out.println("121 isn't a value");
        if(dictionary.getKey(301) != null) 	System.out.println("301 isn't a value");
        
        
        dictionary.clear();
        if(!dictionary.isEmpty())		System.out.println("The dictionary is empty");
        
        System.out.println("End of program, if there is nothing above this line, everything works");
   
        
        
        
        /*for(int i=0; i < SIZE; i++) 
            array[i] = (i+1);
        for(int i=0; i < SIZE; i++) {
            int index = (int)(SIZE*Math.random());
            int tmp = array[i];            
            array[i] = array[index];
            array[index] = tmp;
            }                        
            
        System.out.println("Adding elements to dictionary");
        start = System.currentTimeMillis();            
        for(int i=0; i < SIZE; i++)
            if(!dictionary.add(array[i] , array[i])) {
                System.out.println("ERROR, insertion failed!");
                System.exit(0);
                }
        stop = System.currentTimeMillis();
        System.out.println("Time for insertion of " + SIZE + " elements: " +
            (stop-start));                
        if(dictionary.size() != SIZE)
            System.out.println("ERROR in size(), should be " + SIZE +
                " but the method retured " + dictionary.size());
                
        System.out.println("Now doing lookups");  
        start = System.currentTimeMillis();              
        for(int i=0; i < SIZE; i++) {
            Integer tmp = dictionary.getValue(array[i]);
            if(tmp == null) {
                System.out.println("ERROR, getValue failed!");
                System.exit(0);
                }
            }
        stop = System.currentTimeMillis();
        System.out.println("Time for getValue with " + SIZE + " elements: " +
            (stop-start));            
            
        for(int i=0; i < 100; i++) {
            Integer tmp = dictionary.getKey(array[i]);
            if(tmp == null) {
                System.out.println("ERROR, getKey failed!");
                System.exit(0);
                }
            } 
        
        System.out.println("Now Doing deletion");  
        start = System.currentTimeMillis();          
        for(int i=0; i < SIZE; i++)
            if(!dictionary.delete(array[i])) {
                System.out.println("ERROR, deletion failed!");
                System.exit(0);
                } 
        stop = System.currentTimeMillis();
        System.out.println("Time for deletion with " + SIZE + " elements: " +
            (stop-start));        
                
        if(dictionary.size() != 0)
            System.out.println("ERROR in size(), should be 0 " +
                " but the method retured " + dictionary.size());  
                
        for(int i=0; i < SIZE; i++) {
            Integer tmp = dictionary.getValue(array[i]);
            if(tmp != null) {
                System.out.println("ERROR, getValue failed, found a deleted value at index " + i+"!");
                System.exit(0);
                }
            }  
            
        dictionary.clear();
        
        for(int i=1; i <= 10; i++)
            dictionary.add(i,i);                                                 
                     
        Iterator<Integer> keys = dictionary.keys();
        Iterator<Integer> values = dictionary.values();
        
        System.out.println("The iterators should print 1 .. 10");
        while(keys.hasNext()) {
            System.out.print(keys.next());
            System.out.print("   " + values.next());
            System.out.println();
        }        
        
        try {
            keys = dictionary.keys();
            values = dictionary.values();
            dictionary.add(100,100);   // add element to taint iterators         
            while(keys.hasNext()) {
                Integer tmp = keys.next();
                Integer tmp2 = values.next();
                System.out.println("ERROR, iterator is not fail-fast");
                }       
            }
        catch(ConcurrentModificationException e) {
            System.out.println("OK, iterators are fail-fast");
            }
        catch(Exception e) {
            System.out.println("Iterators are fail-fast, but threw the " +
                "wrong exception " + e);
            }
        dictionary.clear();    
                
        keys = dictionary.keys();
        System.out.println("Now calling iterator on EMPTY structure: ");
        System.out.println("NO output should follow this line ");        
        while(keys.hasNext()) 
            System.out.print(keys.next()+" ");*/                  
    

    }
}