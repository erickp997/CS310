/* Program 4
 * Erick Pulido
 * cssc0934
 */
import java.util.Iterator;
import data_structures.*;
public class PhoneNumber implements Comparable<PhoneNumber> {
    String areaCode, prefix, number;
    String phoneNumber;   
    /*
     * Constructor builds an instance of PhoneNumber, the paremeter is a
     * String. Calls verify to validate the String is in the form xxx-xxx-xxxx
     * which is the area code-prefix-number of a phone number.
     */
    public PhoneNumber(String n) {
        verify(n);
        phoneNumber = n;
        areaCode = n.substring(0,3);
        prefix = n.substring(4,7);
        number = n.substring(8);
    }    
    /* Meet specs of Comparable interface. */ 
    public int compareTo(PhoneNumber n) { 
        return phoneNumber.compareTo(n.phoneNumber); 
    }       
    /* Get the hashCode representation of your phone number. */
    public int hashCode() { return phoneNumber.hashCode(); }
    /* Validate that the phone number is in fact in the correct format.*/
    private void verify(String n) {
        String validPattern = "(\\d{3})-(\\d{3})-(\\d{4})"; // xxx-xxx-xxxx
        if (!n.matches(validPattern)) // is the entry in the right format
            throw new IllegalArgumentException();
    }
    /* Return the area code portion of the phone number. */
    public String getAreaCode() { return areaCode; }
    /* Return the prefix portion of the phone number. */
    public String getPrefix() { return prefix; }
    /* Return the number (last 4 digits) of the phone number. */
    public String getNumber() { return number; }
    /* Return the entire phone number. */
    public String toString() { return phoneNumber; }
}            