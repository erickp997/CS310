/* Program 4
 * Erick Pulido
 * cssc0934
 */
package data_structures;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.ConcurrentModificationException;
public class Hashtable<K extends Comparable<K>,V> implements DictionaryADT<K,V> {
    private int currentSize, maxSize, tableSize;
    private long modCounter;
    private LinearListADT<DictionaryNode<K,V>>[] list;
    /*
     * Constructor builds an instance of Hashtable that is 30% larger than
     * user specified size. Constructs an array of linked lists in order to
     * utilize the chaining strategy.
     */
    public Hashtable(int size) {
        maxSize = size;
        currentSize = 0;
        modCounter = 0;
        tableSize = (int)(maxSize*1.3f);
        list = new LinearList[tableSize];
        for (int i = 0; i < tableSize; i++)
            list[i] = new LinearList<DictionaryNode<K,V>>();
    }
    /* Inner class creating the nodes that populate the hashtable.*/
    class DictionaryNode<K extends Comparable<K>,V> 
                    implements Comparable<DictionaryNode<K,V>> {
        K key;
        V value;
        public DictionaryNode(K k, V v) {
            key = k;
            value = v;
        }
        public int compareTo(DictionaryNode<K,V> node) {
            return key.compareTo(node.key);
        }
    }
    /* Returns true if hashtable has an object with key, false otherwise.*/
    public boolean contains(K key) {
        DictionaryNode<K,V> tmp = new DictionaryNode<K,V>(key,null);
        return list[getIndex(key)].contains(tmp);
    }
    /*
     * Return true if the entry was added successfully. Returns false if the
     * key is a duplicate or if the hashtable is full.
     */
    public boolean add(K key, V value) {
        if (isFull()) return false; // can't add to full table
        if(contains(key)) return false; //no duplicates
        DictionaryNode<K,V> newNode = new DictionaryNode<K,V>(key, value);
        list[getIndex(key)].addLast(newNode);
        currentSize++;
        modCounter++;
        return true;
    }
    /* Returns a positive integer that serves as an index for the hashtable.*/
    private int getIndex(K key) { 
        return (key.hashCode() & 0x7FFFFFFF) % tableSize; 
    }
    /*
     * Returns true if the entry was found and removed from hashtable or
     * returns false if it was not found. 
     */
    public boolean delete(K key) {
        if (!contains(key)) return false;
        list[getIndex(key)].remove(new DictionaryNode<K,V>(key,null));
        currentSize--;
        modCounter++;
        return true;
    }
    /*
     * Returns the value associated with the key and returns null if the key
     * was not found.
     */
    public V getValue(K key) {
        DictionaryNode<K,V> tmp = 
                list[getIndex(key)].find(new DictionaryNode<K,V>(key,null));
        if (tmp == null) return null;
        return tmp.value;
    }
    /*
     * Returns the key associated with value and returns null if the value
     * was not found. Uses Comparable cast on value in order to correctly 
     * compare the values and find matches, if there is one. 
     */
    public K getKey(V value) {
        for (int i = 0; i < tableSize; i++) {
            for (DictionaryNode<K,V> dn : list[i])
                if (((Comparable<V>)value).compareTo(dn.value) == 0)
                    return dn.key; // the key has been found
        }
        return null;
    }
    /* Returns the current size of hashtable*/
    public int size() { return currentSize; }
    /* Returns true if hasthable is at full capicty, false otherwise.*/
    public boolean isFull() { return currentSize == maxSize; }
    /* Returns true if hasthable is empty, false otherwise.*/
    public boolean isEmpty() { return currentSize == 0; }
    /* Returns the hashtable to an empty state.*/
     public void clear() {
         for (int i = 0; i < tableSize; i++) list[i].clear();
         currentSize = 0;
         modCounter++;
    }
     /* Returns a fail-fast Iterator of the keys in ascending order.*/
    public Iterator keys() { return new keyIteratorHelper(); }
    /* Returns a fail-fast Iterator of the values, ordered by keys.*/
    public Iterator values() { return new valueIteratorHelper(); }
    /* IteratorHelper class with inheritance, provided in lecture notes.*/
    abstract class IteratorHelper<E> implements Iterator<E> {
        protected DictionaryNode<K,V>[] tmpArray;
        protected int index;
        protected long modCheck;
        public IteratorHelper() {
            tmpArray = new DictionaryNode[currentSize];
            index = 0;
            int j = 0;
            modCheck = modCounter;
            for (int i = 0; i < tableSize; i++)
                for (DictionaryNode tmp : list[i])
                    tmpArray[j++] = tmp;
            sort();
        }
        /* Performs Shell Sort, provided in lecture notes.*/
        private void sort() {
            DictionaryNode<K,V> tmp;
            int in, out, h = 1;
            int size = tmpArray.length;
            while (h <= size/3) // calculate the gaps
                h = h * 3 + 1;
            while (h > 0) {
                for(out = h; out < size; out++) {
                    tmp = tmpArray[out];
                    in = out;
                    while (in > h-1 && tmpArray[in-h].compareTo(tmp) >= 0) {
                        tmpArray[in] = tmpArray[in-h];
                        in-=h;
                    }
                    tmpArray[in] = tmp;
                }
                h = (h-1)/3;
            }
        }
        public boolean hasNext() {
            if (modCheck != modCounter)
                throw new ConcurrentModificationException();
            return index < currentSize;
        }
        public abstract E next();
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
    class keyIteratorHelper<K> extends IteratorHelper<K> {
        public keyIteratorHelper() {
            super();
        }
        public K next() {
            if (!hasNext())
                throw new NoSuchElementException();
            return (K) tmpArray[index++].key;
        }
    }
    class valueIteratorHelper<V> extends IteratorHelper<V> {
        public valueIteratorHelper() {
            super();
        }
        public V next() {
            if (!hasNext())
                throw new NoSuchElementException();
            return (V) tmpArray[index++].value;
        }
    }
}