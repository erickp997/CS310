/*  Program 2
 *  Erick Pulido
 *  cssc0934
*/
package data_structures;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
public class LinearList<E extends Comparable<E>> implements LinearListADT<E> {
    private Node<E> head, tail;
    private int currentSize;
    private long modCounter;    
    public LinearList() { 
        head = tail = null;
        currentSize = 0;
        modCounter = 0;
    }
    class Node<T> {
        T data;
        Node<T> next, previous, current;       
        Node(T obj) {
            data = obj;
            next = null;
        }
    }
    public boolean addFirst(E obj) {
        Node<E> newNode = new Node<E>(obj);
        if (head == null) head = tail = newNode; //add to empty list
        else { // more than one item
            newNode.next = head;
            head.previous = newNode;
            head = newNode; // the new head
        }
        currentSize++;
        modCounter++;
        return true;
    }
    public boolean addLast(E obj) {
        Node<E> newNode = new Node<E>(obj);
        if (head == null) head = tail = newNode; 
        else {
            newNode.previous = tail;
            tail.next = newNode;
            tail = newNode;
        }
        currentSize++;
        modCounter++;
        return true;
    }
    public E removeFirst() {
        E tmp;
        if (head == null) return null; // checks if it is empty and not found
        if (head == tail) { // if there is one item
            tmp = head.data;
            head = tail = null;
        }
        else { //if there is more than one item in the list
            tmp = head.data;
            head.next.previous = null;
            head = head.next;
        }
        currentSize--;
        modCounter++;
        return tmp;
    }
    public E removeLast() {
        E tmp;
        if (head == null) return null;
        if (head == tail) {
            tmp = head.data;
            head = tail = null;
        }
        else {
            tmp = tail.data;
            tail.previous.next = null;
            tail = tail.previous;
        }
        currentSize--;
        modCounter++;
        return tmp;
    }
    public E remove(E obj) {
        Node<E> current = head;
        while (current != null && obj.compareTo(current.data) != 0)
            current = current.next;
        if (current == head) return removeFirst();
        if (current == null) return null;
        if (current == tail) return removeLast();
        current.previous.next = current.next; //list has 3 or more items
        current.next.previous = current.previous;
        currentSize--;
        modCounter++;
        return current.data;
    }
    public E peekFirst() { return head.data; }
    public E peekLast() { return tail.data; }
    public boolean contains(E obj) { return find(obj) != null; }
    public E find(E obj) {
        for (E tmp : this) {
            if (obj.compareTo(tmp) == 0) {
                return tmp;
            }
        }
        return null;
    }
    public void clear() {
        head = tail = null;
        currentSize = 0;
        modCounter++;
    }
    public boolean isEmpty() { return currentSize == 0; }
    public boolean isFull() { return false; }
    public int size() { return currentSize; }   
    public Iterator<E> iterator() { return (Iterator<E>) new IteratorHelper(); }
    class IteratorHelper implements Iterator<E> {
        private Node<E> iteratorPointer;
        private long modCheck;
        public IteratorHelper() {
            modCheck = modCounter;
            iteratorPointer = head;
        }
        public boolean hasNext() {
            if (modCheck != modCounter)
                throw new ConcurrentModificationException();
            return iteratorPointer != null; 
        }
        public E next() {
            if (!hasNext())
                throw new NoSuchElementException();
            E tmp = iteratorPointer.data;
            iteratorPointer = iteratorPointer.next;
            return tmp;
        }       
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}