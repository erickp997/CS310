/* Program 4
 * Erick Pulido
 * cssc0934
 */
package data_structures;
import java.util.Iterator;
import java.util.TreeMap;
public class BalancedTree<K extends Comparable<K>,V> implements DictionaryADT<K,V> {
    TreeMap<K,V> balancedTree;
    /* Constructor creates an instance of a Red/Black Tree*/
    public BalancedTree() { balancedTree = new TreeMap<K,V>(); }
    /* Returns true if R/B Tree has an object with the key, false otherwise.*/
    public boolean contains(K key) { return balancedTree.containsKey(key); } 
    /*
     * Return true if the entry was added successfully. Returns false if the
     * key is a duplicate or if the hashtable is full.
     */
    public boolean add(K key, V value) { 
        balancedTree.put(key, value);
        return true;
    }
    /*
     * Returns true if the entry was found and removed from hashtable or
     * returns false if it was not found. 
     */
    public boolean delete(K key) { return balancedTree.remove(key) != null; }
    /*
     * Returns the value associated with the key and returns null if the key
     * was not found.
     */
    public V getValue(K key) { return balancedTree.get(key); }
    /*
     * Returns the key associated with value and returns null if the value
     * was not found.
     */
    public K getKey(V value) {
        Iterator<K> keys = keys();
        Iterator<V> values = values();
        while (values.hasNext()) {
            V tmp = values.next(); // hold value from iterator to compare
            int comp = (((Comparable<V>)tmp).compareTo(value));
            if (comp == 0) {
                K keyTmp = keys.next();
                return keyTmp; // the corresponding key to return
            }
            keys.next();
        }
        return null;
    }
    /* Return the current size of the R/B Tree.*/
    public int size() { return balancedTree.size(); }
    /* Returns false always since a R/B Tree is never full.*/
    public boolean isFull() { return false; }
    /* Returns true if there are no entries in R/B Tree, false otherwise. */
    public boolean isEmpty() { return balancedTree.isEmpty(); }
    /* Returns the R/B Tree to an empty state. */
    public void clear() { balancedTree.clear(); }
    /* Returns a fail-fast Iterator of the keys in ascending order.*/
    public Iterator keys() { return balancedTree.keySet().iterator(); }
    /* Returns a fail-fast Iterator of the values, ordered by keys*/
    public Iterator values() { return balancedTree.values().iterator(); }
}