/* Program 4
 * Erick Pulido
 * cssc0934
 */
package data_structures;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.ConcurrentModificationException;
public class BinarySearchTree<K extends Comparable<K>,V> implements DictionaryADT<K,V> {
    private Node<K,V> root;
    private int currentSize;
    private long modCounter;
    private K foundKey;
    /* Constructor creates an instance of BinarySearchTree. */
    public BinarySearchTree() {
        root = null;
        foundKey = null; // temp for reverse lookup
        currentSize = 0;
        modCounter = 0;
    }
    /* Inner class creating the tree nodes that will populate the BST.*/
    class Node<K,V> {
        private K key;
        private V value;
        private Node<K,V> leftChild;
        private Node<K,V> rightChild;
        public Node (K k, V v) {
            key = k;
            value = v;
            leftChild = rightChild = null;
        }
    }
    /* Returns true if BST has an object with the key, false otherwise.*/
    public boolean contains(K key) { return getValue(key) != null; }   
    /*
     * Return true if the entry was added successfully. Returns false if the
     * key is a duplicate or if the hashtable is full.
     */
    public boolean add(K key, V value) {
        if(contains(key)) return false; // no duplicates allowed
        if (root == null) root = new Node<K,V>(key, value);
        else insert(key, value, root, null, false);
        currentSize++;
        modCounter++;
        return true;
    }
    /* Recursively searches where to insert tree node and inserts.*/
    private void insert(K key, V value, Node<K,V> node, Node<K,V> parent, boolean wasLeft) {
        if (node == null) {
            if (wasLeft) parent.leftChild = new Node<K,V>(key, value);
            else parent.rightChild = new Node<K,V>(key, value);
        }
        else if(key.compareTo(node.key) < 0) insert(key, value, node.leftChild, node, true);
        else insert(key, value, node.rightChild, node, false);
    }  
    /*
     * Returns true if the entry was found and removed from BST or returns
     * false if it was not found. 
     */
    public boolean delete(K key) {
        if(isEmpty()) return false;
        if(!remove(key, root, null, false)) return false;
        currentSize--;
        modCounter++;
        return true;
    }
    /*
     * Recursively search for the node to delete, if it exists. If it is found
     * test how many children it has and handle the deletion accordingly. In
     * the case of two children, the node is rewritten with the
     */
    private boolean remove(K key, Node<K,V> node, Node<K,V> parent, boolean wasLeft) {
        if (node == null) return false;
        else if (key.compareTo(node.key) < 0) return remove(key, node.leftChild, node, true);
        else if (key.compareTo(node.key) > 0) return remove(key, node.rightChild, node, false);
        else {
            if (node.leftChild == null && node.rightChild == null) { // no children
                if (parent == null) root = null; // root node with no children
                else if (wasLeft) parent.leftChild = null; // node is a left child
                else parent.rightChild = null; // node is a right child
            }
            else if (node.leftChild == null) { // one right child
                if (parent == null) root = node.rightChild; 
                else if (wasLeft) parent.leftChild = node.rightChild;
                else parent.rightChild = node.rightChild;
            }
            else if (node.rightChild == null) { // one left child
                if (parent == null) root = node.leftChild;
                else if (wasLeft) parent.leftChild = node.leftChild;
                else parent.rightChild = node.leftChild;
            }
            else { // two children, remove using the rewrite strategy
                Node<K,V> successorNode = inorderSuccessor(node);
                rewriteNode(node, successorNode);
            }
        }
        return true;
    }
    /* 
     * Finds and returns successor node of tree node to delete by
     * going right and then as far left as possible. If the inorder
     * successor is not found in that process, return null.
     */
    private Node<K,V> inorderSuccessor(Node<K,V> node) {
        Node<K,V> parent = null;
        Node<K,V> current = node.rightChild; // go right
        while (current.leftChild != null) { // now go as far left
            parent = current;
            current = current.leftChild;
        }
        if (parent == null) return null; // rightChild is inorder successor
        else parent.leftChild = current.rightChild; 
        return current;
    }
    /*
     * Rewrites a node with two children with the inorder successor which
     * effectively deletes the node. If inorderSuccessor returned null
     * then the rightChild must be the inorder successor and the node to
     * delete gets the key, value and also rightChild of rightChild.
     */
    private void rewriteNode(Node<K,V> node, Node<K,V> successor) {
        if (successor == null) { // rightChild is inorder successor
            Node<K,V> newSuccessorNode = node.rightChild;
            node.key = newSuccessorNode.key;
            node.value = newSuccessorNode.value;
            node.rightChild = newSuccessorNode.rightChild; // delete successor
        }
        else { // node is overwritten by inorder successor
            node.key = successor.key;
            node.value = successor.value;
        }
    }
    /*
     * Returns the value associated with the key and returns null if the key
     * was not found.
     */
    public V getValue(K key) { return find(key, root); }
    /* Recursively walk the tree and return the value if found*/
    private V find(K key, Node<K,V> n) {
        if (n == null) return null;
        int comp = key.compareTo(n.key); // keep if statements short
        if (comp < 0) return find(key, n.leftChild); // go left
        if (comp > 0) return find(key, n.rightChild); // go right
        return n.value; // found
    }   
    /*
     * Returns the key associated with value and returns null if the value
     * was not found.
     */
    public K getKey(V value) { 
        foundKey = null;
        find(value, root);
        return foundKey;
    }
    /* 
     * Recursively walk the tree and return the key if found. 
     * Set the temporary foundKey to the key found.
     */
    private void find(V value, Node<K,V> n) {
        if (n == null) return;
        int comp = (((Comparable<V>)value).compareTo(n.value));
        if (comp == 0) foundKey = n.key;
        // travel left and travel right on the tree
        find(value, n.leftChild);
        find(value, n.rightChild);
    }    
    /* Return the current size of the BST.*/
    public int size() { return currentSize; }   
    /* Returns false always since a BST is never full.*/
    public boolean isFull() { return false; } 
    /* Returns true if there are no entries in the BST, false otherwise. */
    public boolean isEmpty() { return currentSize == 0; }   
    /* Returns the BST to an empty state. */
    public void clear() {
        root = null;
        currentSize = 0;
        modCounter++;
    }   
    /* Returns a fail-fast Iterator of the keys in ascending order.*/
    public Iterator keys() { return new keyIteratorHelper(); }   
    /* Returns a fail-fast Iterator of the values, ordered by keys*/
    public Iterator values() { return new valueIteratorHelper(); }
    /* IteratorHelper class that uses inheritance, provided in lecture notes.*/
    abstract class IteratorHelper<E> implements Iterator<E> {
        protected Node<K,V>[] array;
        protected int index, j;
        protected long modCheck;
        public IteratorHelper() {
            array = new Node[currentSize];
            index = j = 0;
            modCheck = modCounter;
            walk(root);
        }
        /* Traverse the tree inorder.*/
        private void walk(Node<K,V> node) {
            if (node == null) return;
            walk(node.leftChild);
            array[j++] = node;
            walk(node.rightChild);
        }
        public boolean hasNext() {
            if (modCheck != modCounter)
                throw new ConcurrentModificationException();
            return index < currentSize;
        }
        public abstract E next();
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
    class keyIteratorHelper<K> extends IteratorHelper<K> {
        public keyIteratorHelper() {
            super();
        }
        public K next() {
            if (!hasNext())
                throw new NoSuchElementException();
            return (K) array[index++].key;
        }
    }
    class valueIteratorHelper<V> extends IteratorHelper<V> {
        public valueIteratorHelper() {
            super();
        }
        public V next() {
            if (!hasNext())
                throw new NoSuchElementException();
            return (V) array[index++].value;
        }
    }
}