import java.util.Iterator;
import java.util.ConcurrentModificationException;
import data_structures.*;

public class BinarySearchTreeTester {
    public static void main(String [] args) {
        new BinarySearchTreeTester();
    }
    public BinarySearchTreeTester() {
        final int SIZE = 1000000;
        long start, stop;
        int [] array = new int[SIZE];
        DictionaryADT<Integer,Integer> dictionary = 
          new BalancedTree<Integer,Integer>();                 
        for(int i=0; i < SIZE; i++) 
            array[i] = (i+1);
        for(int i=0; i < SIZE; i++) {
            int index = (int)(SIZE*Math.random());
            int tmp = array[i];            
            array[i] = array[index];
            array[index] = tmp;
            }                        
            
        System.out.println("Adding elements to dictionary");
        start = System.currentTimeMillis();            
        for(int i=0; i < SIZE; i++)
            if(!dictionary.add(array[i] , array[i])) {
                System.out.println("ERROR, insertion failed!");
                System.exit(0);
                }
        stop = System.currentTimeMillis();
        System.out.println("Time for insertion of " + SIZE + " elements: " +
            (stop-start));                
        if(dictionary.size() != SIZE)
            System.out.println("ERROR in size(), should be " + SIZE +
                " but the method retured " + dictionary.size());
                
        System.out.println("Now doing lookups");  
        start = System.currentTimeMillis();              
        for(int i=0; i < SIZE; i++) {
            Integer tmp = dictionary.getValue(array[i]);
            if(tmp == null) {
                System.out.println("ERROR, getValue failed!");
                System.exit(0);
                }
            }
        stop = System.currentTimeMillis();
        System.out.println("Time for getValue with " + SIZE + " elements: " +
            (stop-start));            
            
        for(int i=0; i < 100; i++) {
            Integer tmp = dictionary.getKey(array[i]);
            if(tmp == null) {
                System.out.println("ERROR, getKey failed!");
                System.exit(0);
                }
            } 
        
        System.out.println("Now Doing deletion");  
        start = System.currentTimeMillis();          
        for(int i=0; i < SIZE; i++)
            if(!dictionary.delete(array[i])) {
                System.out.println("ERROR, deletion failed!");
                System.exit(0);
                } 
        stop = System.currentTimeMillis();
        System.out.println("Time for deletion with " + SIZE + " elements: " +
            (stop-start));        
                
        if(dictionary.size() != 0)
            System.out.println("ERROR in size(), should be 0 " +
                " but the method retured " + dictionary.size());  
                
        for(int i=0; i < SIZE; i++) {
            Integer tmp = dictionary.getValue(array[i]);
            if(tmp != null) {
                System.out.println("ERROR, getValue failed, found a deleted value at index " + i+"!");
                System.exit(0);
                }
            }  
            
        dictionary.clear();
        
        for(int i=1; i <= 10; i++)
            dictionary.add(i,i);                                                 
                     
        Iterator<Integer> keys = dictionary.keys();
        Iterator<Integer> values = dictionary.values();
        
        System.out.println("The iterators should print 1 .. 10");
        while(keys.hasNext()) {
            System.out.print(keys.next());
            System.out.print("   " + values.next());
            System.out.println();
        }        
        
        try {
            keys = dictionary.keys();
            values = dictionary.values();
            dictionary.add(100,100);   // add element to taint iterators         
            while(keys.hasNext()) {
                Integer tmp = keys.next();
                Integer tmp2 = values.next();
                System.out.println("ERROR, iterator is not fail-fast");
                }       
            }
        catch(ConcurrentModificationException e) {
            System.out.println("OK, iterators are fail-fast");
            }
        catch(Exception e) {
            System.out.println("Iterators are fail-fast, but threw the " +
                "wrong exception " + e);
            }
        dictionary.clear();    
                
        keys = dictionary.keys();
        System.out.println("Now calling iterator on EMPTY structure: ");
        System.out.println("NO output should follow this line ");        
        while(keys.hasNext()) 
            System.out.print(keys.next()+" ");
        System.out.println("No keys, values were printed. Iterator on empty worked");
        
        ////////////////////////////////////////////
        //                      17
        //              11              23
        //          8       12      18      25
        //      4      9       13      21 24   30
        //
        //
        ////////////////////////////////////////////
        System.out.println();
        
        System.out.println("Adding to tree again");
        // adding to tree
        if (!dictionary.add(17, 17)) System.out.println("Error in add with 17");
        if (!dictionary.add(11, 11)) System.out.println("Error in add with 11");
        if (!dictionary.add(23, 23)) System.out.println("Error in add with 23");
        if (!dictionary.add(8, 8)) System.out.println("Error in add with 8");
        if (!dictionary.add(12, 12)) System.out.println("Error in add with 12");
        if (!dictionary.add(18, 18)) System.out.println("Error in add with 18");
        if (!dictionary.add(25, 25)) System.out.println("Error in add with 25");
        if (!dictionary.add(4, 4)) System.out.println("Error in add with 4");
        if (!dictionary.add(9, 9)) System.out.println("Error in add with 9");
        if (!dictionary.add(13, 13)) System.out.println("Error in add with 13");
        if (!dictionary.add(21, 21)) System.out.println("Error in add with 21");
        if (!dictionary.add(24, 24)) System.out.println("Error in add with 24");
        if (!dictionary.add(30, 30)) System.out.println("Error in add with 30");
        if (!dictionary.add(23, 23)) System.out.println("Did not add duplicate!");
        if(dictionary.size() != 13) System.out.println("Error size is " + dictionary.size() + " instead of 13");
        
        keys = dictionary.keys();
        values = dictionary.values();
        
        // printing the values in order
        System.out.println("The iterators should print 4 8 9 11 12 13 17 18 23 24 25 30");
        while(keys.hasNext()) {
            System.out.print(keys.next());
            System.out.print("   " + values.next());
            System.out.println();
        }        
        // testing getValue
        if (dictionary.getValue(17) != 17) System.out.println("Error in getValue, did not return 17");
        if (dictionary.getValue(11) != 11) System.out.println("Error in getValue, did not return 11");
        if (dictionary.getValue(8) != 8) System.out.println("Error in getValue, did not return 8");
        if (dictionary.getValue(12) != 12) System.out.println("Error in getValue, did not return 12");
        if (dictionary.getValue(23) != 23) System.out.println("Error in getValue, did not return 23");
        if (dictionary.getValue(18) != 18) System.out.println("Error in getValue, did not return 18");
        if (dictionary.getValue(25) != 25) System.out.println("Error in getValue, did not return 25");
        if (dictionary.getValue(4) != 4) System.out.println("Error in getValue, did not return 4");
        if (dictionary.getValue(9) != 9) System.out.println("Error in getValue, did not return 9");
        if (dictionary.getValue(13) != 13) System.out.println("Error in getValue, did not return 13");
        if (dictionary.getValue(21) != 21) System.out.println("Error in getValue, did not return 21");
        if (dictionary.getValue(24) != 24) System.out.println("Error in getValue, did not return 24");
        if (dictionary.getValue(30) != 30) System.out.println("Error in getValue, did not return 30");
        
        // testing getKey
        if (dictionary.getKey(17) != 17) System.out.println("Error in getKey, returned: " + dictionary.getKey(17) + " instead of 17");
        if (dictionary.getKey(11) != 11) System.out.println("Error in getKey, returned: " + dictionary.getKey(11) + " instead of 11");
        if (dictionary.getKey(8) != 8) System.out.println("Error in getKey, returned: " + dictionary.getKey(8) + " instead of 8");
        if (dictionary.getKey(12) != 12) System.out.println("Error in getKey, returned: " + dictionary.getKey(12) + " instead of 12");
        if (dictionary.getKey(23) != 23) System.out.println("Error in getKey, returned: " + dictionary.getKey(23) + " instead of 23");
        if (dictionary.getKey(18) != 18) System.out.println("Error in getKey, returned: " + dictionary.getKey(18) + " instead of 18");
        if (dictionary.getKey(25) != 25) System.out.println("Error in getKey, returned: " + dictionary.getKey(25) + " instead of 25");
        if (dictionary.getKey(4) != 4) System.out.println("Error in getKey, returned: " + dictionary.getKey(4) + " instead of 4");
        if (dictionary.getKey(9) != 9) System.out.println("Error in getKey, returned: " + dictionary.getKey(9) + " instead of 9");
        if (dictionary.getKey(13) != 13) System.out.println("Error in getKey, returned: " + dictionary.getKey(13) + " instead of 13");
        if (dictionary.getKey(21) != 21) System.out.println("Error in getKey, returned: " + dictionary.getKey(21) + " instead of 21");
        if (dictionary.getKey(24) != 24) System.out.println("Error in getKey, returned: " + dictionary.getKey(24) + " instead of 24");
        if (dictionary.getKey(30) != 30) System.out.println("Error in getKey, returned: " + dictionary.getKey(30) + " instead of 30");
        
        // delete the tree
        if (!dictionary.delete(17)) System.out.println("Error did not delete 17, the root, properly");
        if (!dictionary.delete(11)) System.out.println("Error did not delete 11 properly");
        if (!dictionary.delete(8)) System.out.println("Error did not delete 8 properly");
        if (!dictionary.delete(12)) System.out.println("Error did not delete 12 properly");
        if (!dictionary.delete(23)) System.out.println("Error did not delete 23 properly");
        if (!dictionary.delete(18)) System.out.println("Error did not delete 18 properly");
        if (!dictionary.delete(25)) System.out.println("Error did not delete 25 properly");
        if (!dictionary.delete(4)) System.out.println("Error did not delete 4 properly");
        if (!dictionary.delete(9)) System.out.println("Error did not delete 9 properly");
        if (!dictionary.delete(13)) System.out.println("Error did not delete 13 properly");
        if (!dictionary.delete(21)) System.out.println("Error did not delete 21 properly");
        if (!dictionary.delete(24)) System.out.println("Error did not delete 24 properly");
        if (!dictionary.delete(30)) System.out.println("Error did not delete 25 properly");
        
        // test getKey after remove
        if (dictionary.getKey(17) != null) System.out.println("Error in getKey, returned: " + dictionary.getKey(17) + " instead of 17");
        if (dictionary.getKey(11) != null) System.out.println("Error in getKey, returned: " + dictionary.getKey(11) + " instead of 11");
        if (dictionary.getKey(8) != null) System.out.println("Error in getKey, returned: " + dictionary.getKey(8) + " instead of 8");
        if (dictionary.getKey(12) != null) System.out.println("Error in getKey, returned: " + dictionary.getKey(12) + " instead of 12");
        if (dictionary.getKey(23) != null) System.out.println("Error in getKey, returned: " + dictionary.getKey(23) + " instead of 23");
        if (dictionary.getKey(18) != null) System.out.println("Error in getKey, returned: " + dictionary.getKey(18) + " instead of 18");
        if (dictionary.getKey(25) != null) System.out.println("Error in getKey, returned: " + dictionary.getKey(25) + " instead of 25");
        if (dictionary.getKey(4) != null) System.out.println("Error in getKey, returned: " + dictionary.getKey(4) + " instead of 4");
        if (dictionary.getKey(9) != null) System.out.println("Error in getKey, returned: " + dictionary.getKey(9) + " instead of 9");
        if (dictionary.getKey(13) != null) System.out.println("Error in getKey, returned: " + dictionary.getKey(13) + " instead of 13");
        if (dictionary.getKey(21) != null) System.out.println("Error in getKey, returned: " + dictionary.getKey(21) + " instead of 21");
        if (dictionary.getKey(24) != null) System.out.println("Error in getKey, returned: " + dictionary.getKey(24) + " instead of 24");
        if (dictionary.getKey(30) != null) System.out.println("Error in getKey, returned: " + dictionary.getKey(30) + " instead of 30");
        if (dictionary.size() != 0) System.out.println("Size should be 0 but is " + dictionary.size());
        
        System.out.println("Should not print any values anymore");
        keys = dictionary.keys();
        values = dictionary.values();
        
        // should just be last 6 entries
        System.out.println();
        while(keys.hasNext()) {
            System.out.print(keys.next());
            System.out.print("   " + values.next());
            System.out.println();
        }   
  
        System.out.println("End of program");
    }
}