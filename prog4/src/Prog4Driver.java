import data_structures.*;
public class Prog4Driver {

    public static void main(String[] args) {
        // testing PhoneBook
        PhoneBook phonebook = new PhoneBook(20000);
        //PhoneBook phonebook = new PhoneBook(20000);
        phonebook.load("p4_data.txt");
        phonebook.printAll();
        
        System.out.println(phonebook.numberLookup(new PhoneNumber("000-850-5388")));
        System.out.println(phonebook.nameLookup("Lemour, Mike"));
        System.out.println();
        
        System.out.println(phonebook.nameLookup("Trenton, Torri"));
        System.out.println(phonebook.numberLookup(new PhoneNumber("025-429-7897")));
        System.out.println();
        
        System.out.println(phonebook.nameLookup("Smith, Mike"));
        System.out.println(phonebook.numberLookup(new PhoneNumber("063-150-2434")));
        System.out.println();
        
        System.out.println(phonebook.nameLookup("Jacobson, Marie"));
        System.out.println(phonebook.numberLookup(new PhoneNumber("133-971-2800")));
        System.out.println();
        
        System.out.println(phonebook.nameLookup("Pearson, Don"));
        System.out.println(phonebook.numberLookup(new PhoneNumber("195-516-1519")));
        System.out.println();
        
        System.out.println(phonebook.nameLookup("Roberts, Murray"));
        System.out.println(phonebook.numberLookup(new PhoneNumber("265-962-6618")));
        System.out.println();
        
        System.out.println(phonebook.nameLookup("Grund, Jason"));
        System.out.println(phonebook.numberLookup(new PhoneNumber("304-853-0376")));
        System.out.println();
        
        System.out.println(phonebook.nameLookup("Peterson, Jeremy"));
        System.out.println(phonebook.numberLookup(new PhoneNumber("386-572-3591")));
        System.out.println();
        
        System.out.println(phonebook.nameLookup("Roberts, Mike"));
        System.out.println(phonebook.numberLookup(new PhoneNumber("505-600-8090")));
        System.out.println();
        
        System.out.println(phonebook.nameLookup("Bowden, Chris"));
        System.out.println(phonebook.numberLookup(new PhoneNumber("618-181-5387")));
        System.out.println();
        
        System.out.println(phonebook.nameLookup("Trenton, Bill"));
        System.out.println(phonebook.numberLookup(new PhoneNumber("692-494-1183")));
        System.out.println();
        
        System.out.println(phonebook.nameLookup("Roberson, Aaron"));
        System.out.println(phonebook.numberLookup(new PhoneNumber("999-384-8820")));
        System.out.println();
        
        System.out.println(phonebook.nameLookup("Ransom, Don"));
        
        
        System.out.println();
        System.out.println("End of program");
    }
}