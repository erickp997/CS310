
public class Prog4Tester {

    public static void main(String[] args) {
        PhoneBook phonebook = new PhoneBook(100);
        phonebook.load("p4_data.txt");
        phonebook.printAll();
        phonebook.printNames();
        phonebook.printByAreaCode("025");
        System.out.println("Adding Smith, Jerry to phonebook");
        PhoneNumber js = new PhoneNumber("619-555-2576");
        String testEntry = "Smith, Jerry";
        phonebook.addEntry(js, testEntry);
        phonebook.nameLookup(testEntry);
        phonebook.printAll();
        phonebook.printNames();
        System.out.println("Deleting Smith, Jerry from phonebook");
        phonebook.deleteEntry(js);
        phonebook.printAll();
        System.out.println("End of program");
    }
}