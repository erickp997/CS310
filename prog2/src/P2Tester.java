import data_structures.*;
public class P2Tester {
private LinearListADT<Integer> list;    
    public P2Tester() {
        list = new LinearList<Integer>();
        runTests();
    }
    private void runTests() {
        list.addLast(10);
        print ("Adding 10");
        print ("Should now print 10");
        for (int i : list) print(i);
        print("\n");
        list.addLast(7);
        list.addLast(8);
        print ("Adding 7 8");
        print ("Should now print 10 7 8");
        for (int i : list) print(i);
        print("\n");
        list.addFirst(9);
        list.addFirst(3);
        print ("Should now print 3 9 10 7 8");
        for (int i : list) print(i);
        print("\n");
        print("Should now show 3");
        print(list.peekFirst());
        print("\n");
        list.removeFirst();
        print ("Should now print 9 10 7 8");
        for (int i : list) print(i);
        print("\n");
        print ("Should find 10");
        print(list.find(10));
        print("\n");
        print("Should now show 8");
        print(list.peekLast());
        print("\n");
        list.clear();
        print ("Should now print nothing");
        for (int i : list) print(i);
        print("\n");
        print ("Should not find anything");
        print("\n");
        if(list.find(10) != null) print("10 ERROR in find");
        list.addFirst(9);
        list.addLast(17);
        list.addFirst(12);
        print ("Should now print 12 9 17");
        for (int i : list) print(i);
        print("\n");
        list.removeFirst();
        print ("Show now print 9 17");
        for (int i : list) print(i);
        print("\n");
        list.removeLast();
        print ("Should now print 9");
        for (int i : list) print(i);
        print("\n");
        list.remove(9);
        print ("Should now print nothing");
        for (int i : list) print(i);
        print("\n");
        if (list.isEmpty() != true) print("Error in isEmpty");
        list.addLast(19);
        if (list.remove(19) != 19) print("Error in remove");
        list.addFirst(7);
        list.addLast(20);
        if (list.contains(19)) print ("Error in contains");
        if (list.peekLast() != 20) print ("Error in peekLast");
        if (list.find(7) != 7) print ("Error in find");
        if (list.find(29) != null) print ("Error in find");
        print("\nEnd of program");
    }       
    private void print(String s) { System.out.println(s); }    
    private void print(int i) { System.out.print(" " + i); }           
    public static void main(String [] args) { new P2Tester(); }
}