import data_structures.*;
public class P2QueueTester2 {
    private Queue<Integer> queue;
    private static final int MAX_SIZE = 1000000;    
    public P2QueueTester2() {
        queue = new Queue<Integer>();
        runTests();
    }      
    private void runTests() {
        try{    
            for(int i=1; i <= 200; i++) queue.enqueue(i);
            print("1 Should print 1 .. 200");
            print();
            for(int i = 1; i <= 198; i++)
                if(queue.dequeue() != i) // leave two elements
                    print("2 ERROR in dequeue at" + i);
            print("Should print 199 200");
            print(); 
            for(int i=0; i < MAX_SIZE-2; i++) queue.enqueue(-i);
            print("3 Size should be 1000000 and is now: " + queue.size());
            for(int i=0; i < MAX_SIZE >> 2; i++) {
                queue.dequeue();
                queue.dequeue();
            }
            print("\n4 Size should be 500000 and is now: " + queue.size());
            queue = new Queue<Integer>();
        }
        catch(Exception e) {
            System.out.println("In first catch block");
            e.printStackTrace();
        }
        try {    
            queue.makeEmpty();
            print();
            queue.enqueue(5);
            if(queue.peek() != 5) print("5 ERROR in peek");
            if(!queue.remove(5)) print("8 ERROR in remove");
            if(queue.contains(5)) print("9 ERROR in contains");        
            if(!queue.isEmpty()) print("11 ERROR in isEmpty");
            queue.enqueue(15);
            if(queue.peek() != 15) print("12 ERROR in peek");
            if(!queue.remove(15)) print("13 ERROR in remove");
            if(queue.contains(15)) print("14 ERROR in contains");              
        }
        catch(Exception e) {
            System.out.println("16 In second catch block");
            e.printStackTrace();
        } 
        try {
            queue = new Queue<Integer>();    
            for(int i=0; i < MAX_SIZE; i++)
                queue.enqueue(i);     
            for(int j=0; j < 20; j++) {     
                for(int i=0; i < 10; i++)
                    if(queue.dequeue() == null) print("17 ERROR in dequeue");
                for(int i=0; i < 1000; i++)
                    queue.enqueue(i);
            }                  
            for(int j=0; j < 20; j++) {    
                for(int i=0; i < 1000; i++)
                    if(queue.dequeue() == null) print("19 ERROR in dequeue");
                for(int i=0; i < 1000; i++)
                    queue.enqueue(i);
           }                        
        }
        catch(Exception e) {
            System.out.println("24 In third catch block");
            e.printStackTrace();
    }
        print("\nEnd of program"); // Sanity check
    }      
    private void print(String s) { System.out.println(s); }      
    private void print() {
        for(Integer i : queue) System.out.print(i + " ");
        System.out.println();
    }       
    public static void main(String [] args) { new P2QueueTester2(); }
}