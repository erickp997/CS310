import data_structures.*;
public class P2StackTester {
private Stack<Integer> stack;
    public P2StackTester() {
        stack = new Stack<Integer>();
        runTests();
    }
    private void runTests() {
        stack.push(10);
        print ("Adding 10 should now print 10");
        for (int i : stack) print(i);
        print("\n");
        stack.push(7);
        stack.push(8);
        print ("Adding 8 7 should now print 8 7 10");
        for (int i : stack) print(i);
        print("\n");
        stack.push(9);
        stack.push(3);
        print ("Adding 3 9 should now print 3 9 8 7 10");
        for (int i : stack) print(i);
        print("\n");
        print("Should now show 3");
        print(stack.peek());
        print("\n");
        stack.pop();
        print ("Should now print 9 10 7 8");
        for (int i : stack) print(i);
        print("\n");
        print ("Should find 10");
        if(!stack.contains(10)) print ("Error in contains");
        print("\n");
        stack.makeEmpty();
        print ("Should now print nothing");
        for (int i : stack) print(i);
        print("\n");
        print ("Should not find anything");
        print("\n");
        if(stack.contains(10)) print("Error in contains");
        stack.push(9);
        stack.push(17);
        stack.push(12);
        print ("Should now print 12 9 17");
        for (int i : stack) print(i);
        print("\n");
        stack.pop();
        print ("Show now print 9 17");
        for (int i : stack) print(i);
        print("\n");
        stack.pop();
        print ("Should now print 9");
        for (int i : stack) print(i);
        print("\n");
        stack.remove(9);
        print ("Should now print nothing");
        for (int i : stack) print(i);
        print("\n");
        print("\nEnd of program"); // Sanity check
    }
    private void print(String s) { System.out.println(s); }   
    private void print(int i) { System.out.print(" "+i); }          
    public static void main(String [] args) { new P2StackTester(); }
}