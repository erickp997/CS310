import data_structures.*;
public class P2StackTester2 {
    private Stack<Integer> stack;
    private static final int MAX_SIZE = 1000000;    
    public P2StackTester2() {
        stack = new Stack<Integer>();
        runTests();
    }      
    private void runTests() {
        try{    
            for(int i=200; i > 0; i--) stack.push(i);
            print("1 Should print 1 .. 200");
            print();
            for(int i = 1; i <= 198; i++)
                if(stack.pop() != i) // leave two elements
                    print("2 ERROR in pop at" + i);
            print("Should print 199 200");
            print(); 
            for(int i=0; i < MAX_SIZE-2; i++) stack.push(-i);
            print("3 Size should be 1000000 and is now: " + stack.size());
            for(int i=0; i < MAX_SIZE >> 2; i++) {
                stack.pop();
                stack.pop();
            }
            print("\n4 Size should be 500000 and is now: " + stack.size());
            stack = new Stack<Integer>();
        }
        catch(Exception e) {
            System.out.println("In first catch block");
            e.printStackTrace();
        }
        try {    
            stack.makeEmpty();
            print();
            stack.push(5);
            if(stack.peek() != 5) print("5 ERROR in peek");
            if(!stack.remove(5)) print("8 ERROR in remove");
            if(stack.contains(5)) print("9 ERROR in contains");        
            if(!stack.isEmpty()) print("11 ERROR in isEmpty");
            stack.push(15);
            if(stack.peek() != 15) print("12 ERROR in peekLast");
            if(!stack.remove(15)) print("13 ERROR in remove");
            if(stack.contains(15)) print("14 ERROR in contains");              
        }
        catch(Exception e) {
            System.out.println("16 In second catch block");
            e.printStackTrace();
        } 
        try {
            stack = new Stack<Integer>();    
            for(int i=0; i < MAX_SIZE; i++)
                stack.push(i);     
            for(int j=0; j < 20; j++) {     
                for(int i=0; i < 10; i++)
                    if(stack.pop() == null) print("17 ERROR in removeFirst");
                for(int i=0; i < 1000; i++)
                    stack.push(i);
            }                  
            for(int j=0; j < 20; j++) {    
                for(int i=0; i < 1000; i++)
                    if(stack.pop() == null) print("19 ERROR in removeLast");
                for(int i=0; i < 1000; i++)
                    stack.push(i);
           }                        
        }
        catch(Exception e) {
            System.out.println("24 In third catch block");
            e.printStackTrace();
    }
        print("\nEnd of program"); // Sanity check
    }      
    private void print(String s) { System.out.println(s); }      
    private void print() {
        for(Integer i : stack) System.out.print(i + " ");
        System.out.println();
    }       
    public static void main(String [] args) { new P2StackTester2(); }
}