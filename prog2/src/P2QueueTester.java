import data_structures.*;
public class P2QueueTester {
private Queue<Integer> queue;
    public P2QueueTester() {
        queue = new Queue<Integer>();
        runTests();
    }
    private void runTests() {
        queue.enqueue(10);
        print ("Adding 10 should now print 10");
        for (int i : queue) print(i);
        print("\n");
        queue.enqueue(7);
        queue.enqueue(8);
        print ("Adding 8 7 should now print 10 7 8");
        for (int i : queue) print(i);
        print("\n");
        queue.enqueue(9);
        queue.enqueue(3);
        print ("Adding 3 9 should now print 10 7 8 9 3");
        for (int i : queue) print(i);
        print("\n");
        print("Should now show 10");
        print(queue.peek());
        print("\n");
        queue.dequeue();
        print ("Should now print 7 8 9 3");
        for (int i : queue) print(i);
        print("\n");
        print ("Should find 7");
        if(!queue.contains(7)) print ("Error in contains");
        print("\n");
        queue.makeEmpty();
        print ("Should now print nothing");
        for (int i : queue) print(i);
        print("\n");
        print ("Should not find anything");
        print("\n");
        if(queue.contains(10)) print("Error in contains");
        queue.enqueue(9);
        queue.enqueue(17);
        queue.enqueue(12);
        print ("Should now print 9 17 12");
        for (int i : queue) print(i);
        print("\n");
        queue.dequeue();
        print ("Show now print 17 12");
        for (int i : queue) print(i);
        print("\n");
        queue.dequeue();
        print ("Should now print 12");
        for (int i : queue) print(i);
        print("\n");
        queue.remove(12);
        print ("Should now print nothing");
        for (int i : queue) print(i);
        print("\n");
        print("\nEnd of program"); // Sanity Check
    }  
    private void print(String s) { System.out.println(s); }   
    private void print(int i) { System.out.print(" "+ i); }               
    public static void main(String [] args) { new P2QueueTester(); }
}