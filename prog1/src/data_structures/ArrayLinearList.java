/* Erick Pulido
 * cssc0934
 */
package data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayLinearList<E> implements LinearListADT<E> {
    private int front, rear, currentSize, maxSize;
    private E[] array;
    
//  Create ArrayLinearLinst with specific capacity.
    public ArrayLinearList(int maxCapacity) {
        maxSize = maxCapacity;
        front = rear = currentSize = 0;
        array = (E[]) new Object[maxSize];
    } 
    
//  Create ArrayLinearList with default capacity.
    public ArrayLinearList() { this(DEFAULT_MAX_CAPACITY); }

    public boolean addFirst(E obj) {
        if (isFull()) return false;
        if (isEmpty()) { // handles instance of empty array
            front = rear = 0;
            array[front] = obj;
        }
        else {
            front--;
            if (front == -1) front = maxSize - 1; // handles the wrap
            array[front] = obj;
        }
        currentSize++;
        return true;
    }
    
    public boolean addLast(E obj) {
        if (isFull()) return false;
        if (isEmpty()) { // handles instance of empty array
            front = rear = 0;
            array[rear] = obj;
        }
        else {
            rear++;
            if (rear == maxSize) rear = 0; // handles the wrap
            array[rear] = obj;
        }
        currentSize++;
        return true;
    }
    
    public E removeFirst() {
        if (isEmpty()) return null;
        E tmpToRemove = array[front++];
        if (front == maxSize) front = 0;
        currentSize--;
        if (currentSize == 1) front = rear = 0;
        return tmpToRemove;
    }
    
    public E removeLast() {
        if (isEmpty()) return null;
        E tmpToRemove = array[rear--];
        if (rear == -1) rear = maxSize - 1;
        currentSize--;
        if (currentSize == 1) rear = front = 0;
        return tmpToRemove;
    }
    
//  Removes the Object obj from the list if the list contains it. 
//  Returns null if list is empty or not found.
//  Order of list is preserved, if duplicates takes the first matching element.
//  Shifts elements to fill the slot where the element was located.
    public E remove(E obj) {
        int foundIndex = -1;
        int index = front;
        if (isEmpty()) return null; // The list is empty case
        
        // find if the element exists in list
        for (int i = 0; i < currentSize; i++) {
            if (((Comparable<E>)obj).compareTo(array[index]) == 0) {
                foundIndex = index;
                break;
            }
            index++;
            if (index == maxSize) index = 0;
        }
        
        if (foundIndex == -1) return null; // Element was not found        
        E tmpToRemove = array[foundIndex]; // Element found and stored       
//      Using i != rear for easier management than handling situations
//      where i < currentSize - 1 and i never meeting the condition statement.
        for (int i = foundIndex; i != rear; i++) {
            if (i == maxSize - 1) {
                array[maxSize - 1] = array[0];            
                i = -1;  // handles case where i needs to get to 0
            }
            else {
                array[i] = array[i + 1];
            }
        }              
//      after shifts
        --rear;
        if (rear == -1) rear = maxSize - 1;
        currentSize--;        
        return tmpToRemove;
    }
    
//  Return first element of list, null if empty and list stays unchanged.
    public E peekFirst() {
        if (isEmpty()) return null;
        return array[front];
    }
    
//  Return last element of list, null if empty and list stays unchanged.
    public E peekLast() {
        if (isEmpty()) return null;
        return array[rear];
    }
    
//  Returns true if list contains Object obj, false otherwise.
    public boolean contains(E obj) { return find(obj) != null; }

//  Returns the first instance of the element matching obj.
//  Returns null otherwise and list remains unchanged.
    public E find(E obj) {
        for (E tmp: this) {
            if (((Comparable<E>)obj).compareTo(tmp) == 0)
                return tmp;
        }
        return null;
    }
    
//  The list is returned to an empty state.
    public void clear() { front = rear = currentSize = 0; }
    
//  Returns true if the list is empty, otherwise false
    public boolean isEmpty() { return currentSize == 0; }
    
//  Returns true if the list is full, otherwise false
    public boolean isFull() { return currentSize == maxSize; }
    
//  Returns the number of Objects currently in the list.
    public int size() { return currentSize; }
    
//  Returns an Iterator of the values in the list, presented in the 
//  same order as the underlying order of the list. (front first, rear last)
    public Iterator<E> iterator() { return new IteratorHelper(); }
    
    class IteratorHelper implements Iterator<E> {
        private int count, index;
        public IteratorHelper() {
            index = front;
            count = 0;
        }
        
//      Returns true count does not match current size
        public boolean hasNext() { return count != currentSize; }
        
//      Returns the next element in the array if there is one
        public E next() {
            if (!hasNext()) throw new NoSuchElementException();
            E tmp = array[index++];
            if (index == maxSize) index = 0;
            count++;
            return tmp;
        }
        
        public void remove() { throw new UnsupportedOperationException(); }
    }
}