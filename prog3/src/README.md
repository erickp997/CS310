Program 3

Used the MazeGrid and GridCell classes provided to make MazeSolver. MazeSolver finds the shortest possible path, if it exists,
with the help of the stack and queue class from the previous programming assignment. Breadth-First Search and Depth-First Search
was used to solve the randomly generated maze.
