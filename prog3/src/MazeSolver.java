/* Program 3
 * Erick Pulido
 * cssc0934 
 */
import data_structures.*;
public class MazeSolver {
private MazeGrid grid;
private Stack<GridCell> stack;
private Queue<GridCell> queue;
private int gridDimension;
    public MazeSolver(int dimension) {
        grid = new MazeGrid(this, dimension);
        stack = new Stack<GridCell>();
        queue = new Queue<GridCell>();
        gridDimension = dimension;
    }
    public void mark() {
        GridCell startGridCell = grid.getCell(0,0); // create reference to cell(0,0) 
        startGridCell.setDistance(0); // have not travelled yet
        queue.enqueue(startGridCell);
        grid.markDistance(startGridCell); // grid indicates there has not been any travel
        while (!queue.isEmpty()) {
            GridCell currentCell = queue.dequeue();
            int x = currentCell.getX();
            int y = currentCell.getY();
            int distance = currentCell.getDistance();
            GridCell north = grid.getCell(x, y+1); // upper adjacent cell
            GridCell south = grid.getCell(x, y-1); // lower adjacent cell
            GridCell west = grid.getCell(x-1, y); // left adjacent cell
            GridCell east = grid.getCell(x+1, y); // right adjacent cell
            if (grid.isValidMove(north) && !north.wasVisited()) { // cell above currentCell
                north.setDistance(distance + 1); // moved one space over
                queue.enqueue(north);
            }
            if (grid.isValidMove(south) && !south.wasVisited()) { // cell below currentCell
                south.setDistance(distance + 1);
                queue.enqueue(south);
            }
            if (grid.isValidMove(west) && !west.wasVisited()) { // cell left of currentCell
                west.setDistance(distance + 1);
                queue.enqueue(west);
            }
            if (grid.isValidMove(east) && !east.wasVisited()) { // cell right of currentCell
                east.setDistance(distance + 1);
                queue.enqueue(east);
            }
            grid.markDistance(currentCell);
        }
    }
    public boolean move() {
        GridCell terminalCell = grid.getCell(gridDimension-1, gridDimension-1);
        int distance = terminalCell.getDistance();
        if (distance == -1) return false;
        stack.push(terminalCell);
        while(distance != 0) {
            GridCell currentCell = stack.peek(); // first cell should be terminal cell
            int x = currentCell.getX();
            int y = currentCell.getY();
            int currentCellDistance = currentCell.getDistance();
            GridCell testCell = grid.getCell(x, y+1); // north
            GridCell tempCell = testCell; // north is just a placeholder
            if (grid.isValidMove(testCell) && testCell.getDistance() < currentCellDistance) 
                tempCell = testCell;
            testCell = grid.getCell(x, y-1); // south
            if (grid.isValidMove(testCell) && testCell.getDistance() < currentCellDistance)
                tempCell = testCell;
            testCell = grid.getCell(x+1, y); // west
            if (grid.isValidMove(testCell) && testCell.getDistance() < currentCellDistance)
                tempCell = testCell;
            testCell = grid.getCell(x-1, y); // east
            if (grid.isValidMove(testCell) && testCell.getDistance() < currentCellDistance)
                tempCell = testCell;
            stack.push(tempCell); // takes last instance of move preventing duplicates
            distance--;
        }
        while(!stack.isEmpty()) grid.markMove(stack.pop());
        return true;    
    }  
    public void reset() {
        stack.makeEmpty();
        queue.makeEmpty();
    }
    public static void main(String[] args) {
        new MazeSolver(30);
    }
}